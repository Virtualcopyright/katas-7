Array.prototype.forEachCustom = function(callback) {
    let thisArg = arguments[1];

    for (let i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
    }
};
Array.prototype.mapCustom = function(callback) {
    let thisArg = arguments[1];

    let arr = new Array(this.length);
    for (let i = 0; i < this.length; i++) {
        arr[i] = callback.call(thisArg, this[i], i, this);
    }
    return arr;
};
Array.prototype.someCustom = function(callback) {
    let thisArg = arguments[1];

    for (let i = 0; i < this.length; i++) {
        if (i in this && callback.call(thisArg, this[i], i, this))
            return true;
    }
    return false;
};
Array.prototype.findCustom = function(callback) {
    let thisArg = arguments[1];

    for (let i = 0; i < this.length; i++) {
        if (callback.call(thisArg, this[i], i, this))
            return this[i];
    }
};
Array.prototype.findIndexCustom = function(callback) {
    let thisArg = arguments[1];
    
    for (let i = 0; i < this.length; i++) {
        if (callback.call(thisArg, this[i], i, this))
            return i;
    }
    return -1;
};
Array.prototype.everyCustom = function(callback) {
    let thisArg = arguments[1];
    
    for (let i = 0; i < this.length; i++) {
        if (callback.call(thisArg, this[i], i, this) === false)
            return false;
    }
    return true;
};
Array.prototype.filterCustom = function(callback) {
    let thisArg = arguments[1];

    let arr = [];
    for (let i = 0; i < this.length; i++) {
        if (callback.call(thisArg, this[i], i, this))
            arr.push(this[i]);
    }
    return arr;
};