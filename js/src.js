/* Test Cases */
let sum = 0;
let arr = [1,2,3];
//forEach Test:
arr.forEachCustom(x=>sum+=x)
AssertEquals(sum, 6);
//map Test:
AssertEquals(arr.mapCustom(x=>x=1), [1,1,1]);
//some Test:
arr = [1,2,3];
AssertEquals(arr.someCustom(x=>x%2==0), true);
//find Test:
arr = [5, 12, 8, 130, 44];
AssertEquals(arr.findCustom(x=>x>10), 12);
//findIndex Test:
AssertEquals(arr.findIndexCustom(x=>x>10), 1);
//every Test:
arr = [1, 30, 39, 29, 10, 13];
AssertEquals(arr.everyCustom(x=>x<40), true);
//filter Test:
arr = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
AssertEquals(arr.filterCustom(x=>x.length>6), ["exuberant", "destruction", "present"]);

//Test AssertEquals:
AssertEquals(false, true);



function AssertEquals (testCase, expected) {
    if (typeof(expected) == 'object')
        console.assert(testCase.length === expected.length && testCase.sort().every(function(v, i) { return v === expected.sort()[i]}), `Expected: ${expected}, instead got: ${testCase}`);
    else
        console.assert(testCase == expected, `Expected: ${expected}, instead got: ${testCase}`);
}